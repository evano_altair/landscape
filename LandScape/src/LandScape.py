import os
import sys
import time
from random import Random
from typing import Dict, Tuple, Optional
from typing import List, Union, Set

import math
from PIL import Image, ImageDraw


class Parameter:
    MIN_FALLOW_AREA = 2000.0
    MIN_SHALLOW_AREA = 20000.0
    MIN_DEEP_AREA = 6000.0
    MAX_FALLOW_AREA = 12000.0
    MAX_SHALLOW_AREA = float("INF")
    MAX_DEEP_AREA = 24000.0

    MIN_FALLOW_PORTION = 0.15
    MIN_SHALLOW_PORTION = 0.45
    MIN_DEEP_PORTION = 0.25
    MAX_FALLOW_PORTION = 0.225
    MAX_SHALLOW_PORTION = 0.525
    MAX_DEEP_PORTION = 0.325

    FLOAT_PRECISION = 1E-9
    MAX_SOLUTION_NUM = 100
    MAX_SEARCH_TIME = 60  # seconds

    FALLOW_COLOR = "#f7941d"
    SHALLOW_COLOR = "#6dcff6"
    DEEP_COLOR = "#0054a6"
    BLANK_COLOR = "#ffffff"
    OUTLINE_COLOR = "#000000"
    IMAGE_RESOLUTION_SCALE = 1.0

    CHOOSE_DEEP_FIRST = True


class Point:
    x: float
    y: float

    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    def __eq__(self, other):
        if self is other:
            return True
        if not isinstance(other, Point):
            return False
        if math.fabs(self.x - other.x) > Parameter.FLOAT_PRECISION:
            return False
        if math.fabs(self.y - other.y) > Parameter.FLOAT_PRECISION:
            return False
        return True

    def __hash__(self):
        return hash((self.x, self.y))

    def __str__(self):
        return "({0}, {1})".format(self.x, self.y)


class Line:
    a: Point
    b: Point

    def __init__(self, a: Point, b: Point):
        self.a = a
        self.b = b

    def __eq__(self, other):
        if self is other:
            return True
        if not isinstance(other, Line):
            return False
        return (self.a == other.a and self.b == other.b) or (self.a == other.b and self.b == other.a)

    def __hash__(self):
        return hash((self.a, self.b))

    def __str__(self):
        return "[{0}, {1}]".format(self.a, self.b)


class Block:
    BLANK = 0b000
    FALLOW = 0b001
    SHALLOW = 0b010
    DEEP = 0b100

    lines: List[Line]
    points: List[Point]
    area: float

    land_type: int
    at_edge: bool
    neighbors: List["Block"]

    def __init__(self, points: List[Point]):
        self.points = points
        self.lines = list()
        for i in range(len(points)):
            self.lines.append(Line(points[i], points[(i + 1) % len(points)]))
        self.area = self._get_area_()
        self.land_type = Block.BLANK
        self.at_edge = False
        self.neighbors = list()

    def __eq__(self, other):
        if self is other:
            return True
        if not isinstance(other, Block):
            return False
        if len(self.lines) != len(other.lines):
            return False
        same_line_num = 0
        for l1 in self.lines:
            for l2 in other.lines:
                if l1 == l2:
                    same_line_num += 1
        return same_line_num == len(self.lines)

    def __hash__(self):
        return hash(tuple(self.points))

    def __str__(self):
        return str(self.points)

    def _get_area_(self) -> float:
        num = len(self.points)
        area = 0.0
        for i in range(num):
            p0, p1, p2 = self.points[(i - 1) % num], self.points[i], self.points[(i + 1) % num]
            area += p1.y * (p0.x - p2.x)
        return area


class Geometry:

    @staticmethod
    def distance(a: Union[Point, Line], b: Union[Point, Line]) -> float:
        if isinstance(a, Point):
            if isinstance(b, Point):
                return Geometry._point_to_point_distance_(a, b)
            else:
                return Geometry._point_to_line_distance_(a, b)
        else:
            if isinstance(b, Point):
                return Geometry._point_to_line_distance_(b, a)
            else:
                return Geometry._line_to_line_distance_(a, b)

    @staticmethod
    def connected(l1: Line, l2: Line) -> bool:
        return (l1.a == l2.a and l1.b != l2.b) or (l1.a == l2.b and l1.b != l2.a) or (
                l1.b == l2.a and l1.a != l2.b) or (l1.b == l2.b and l1.a != l2.a)

    @staticmethod
    def adjacent(p1: Block, p2: Block) -> bool:
        for l1 in p1.lines:
            if l1 in p2.lines:
                return True
        return False

    @staticmethod
    def _point_to_point_distance_(a: Point, b: Point) -> float:
        return math.sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y))

    @staticmethod
    def _point_to_line_distance_(p: Point, l: Line) -> float:
        a, b = l.a, l.b
        r = ((p.x - a.x) * (b.x - a.x) + (p.y - a.y) * (b.y - a.y)) / (
                (a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y))
        if r <= 0:
            return Geometry.distance(a, p)
        elif r >= 1:
            return Geometry.distance(b, p)
        else:
            ab = Geometry.distance(a, b)
            ac = r * ab
            ap = Geometry.distance(a, p)
            return math.sqrt(ap * ap - ac * ac)

    @staticmethod
    def _line_to_line_distance_(l1: Line, l2: Line) -> float:
        return min(Geometry.distance(l1.a, l2), Geometry.distance(l1.b, l2), Geometry.distance(l2.a, l1),
                   Geometry.distance(l2.b, l1))


class Disjoint:
    land_type: int
    parent: "Disjoint"

    def __init__(self, land_type: int):
        self.land_type = land_type
        self.parent = self

    @property
    def root(self) -> "Disjoint":
        if self.parent is self:
            return self
        self.parent = self.parent.root
        return self.parent

    @classmethod
    def merge(cls, u1: "Disjoint", u2: "Disjoint") -> None:
        r1 = u1.root
        r2 = u2.root
        if r1 != r2:
            r2.parent = r1

    def __hash__(self):
        return hash(self.__repr__())


class Region:
    land_type: int
    blocks: Set[Block]

    def __init__(self, land_type: int, blocks: List[Block]):
        self.land_type = land_type
        self.blocks = set(blocks)

    @property
    def closed(self) -> bool:
        for block in self.blocks:
            for neighbor in block.neighbors:
                if neighbor.land_type == Block.BLANK and (neighbor not in self.blocks):
                    return False
        return True

    def check_area(self, total_area: float) -> bool:
        if self.land_type == Block.FALLOW:
            min_area = Parameter.MIN_FALLOW_AREA
            max_area = Parameter.MAX_FALLOW_AREA
        elif self.land_type == Block.DEEP:
            min_area = Parameter.MIN_DEEP_AREA
            max_area = Parameter.MAX_DEEP_AREA
        else:
            min_area = max(Parameter.MIN_SHALLOW_AREA, total_area * Parameter.MIN_SHALLOW_PORTION)
            max_area = Parameter.MAX_SHALLOW_AREA
        area = 0.0
        for block in self.blocks:
            area += block.area
        if area > max_area:
            return False
        if self.closed and area < min_area:
            return False
        return True


class Operation:

    @staticmethod
    def generate_neighbors(blocks: List[Block]) -> None:
        for i in range(0, len(blocks) - 1):
            b1 = blocks[i]
            for j in range(i + 1, len(blocks)):
                b2 = blocks[j]
                if Geometry.adjacent(b1, b2):
                    b1.neighbors.append(b2)
                    b2.neighbors.append(b1)

    @staticmethod
    def mark_edge_blocks(all_blocks: List[Block], edge_blocks: List[Block]) -> None:
        for edge_block in edge_blocks:
            for block in all_blocks:
                if block == edge_block:
                    block.at_edge = True
                    break

    @staticmethod
    def check_regions(blocks: List[Block], total_area: float) -> bool:
        all_regions = Operation._get_all_regions_(blocks)
        for region in all_regions:
            if not region.check_area(total_area):
                return False
        return True

    @staticmethod
    def _get_all_regions_(blocks: List[Block]) -> List[Region]:
        all_disjoints = dict()  # type: Dict[Block, Disjoint]
        for block in blocks:
            all_disjoints[block] = Disjoint(block.land_type)
        for block, disjoint in all_disjoints.items():
            for neighbor in block.neighbors:
                if block.land_type == neighbor.land_type or (block.land_type | neighbor.land_type) == Block.SHALLOW:
                    Disjoint.merge(disjoint, all_disjoints[neighbor])
        disjoint_block_map = dict()  # type: Dict[Disjoint, List[Block]]
        for block, disjoint in all_disjoints.items():
            root = disjoint.root
            if root not in disjoint_block_map:
                disjoint_block_map[root] = list()
            disjoint_block_map[root].append(block)
        all_regions = list()  # type: List[Region]
        for disjoint, region_blocks in disjoint_block_map.items():
            all_regions.append(Region(disjoint.land_type, region_blocks))
        return all_regions


class Search:
    blocks: List[Block]
    random: Random
    total_area: float
    fallow_area: float
    shallow_area: float
    deep_area: float
    runtime: float
    dice: List[Tuple[int, int, int]]

    def __init__(self, blocks: List[Block]):
        self.blocks = list()
        self.total_area = 0.0
        for block in blocks:
            new_block = Block(block.points)
            new_block.at_edge = block.at_edge
            self.blocks.append(new_block)
            self.total_area += new_block.area
        Operation.generate_neighbors(self.blocks)
        self.dice = [(Block.FALLOW, Block.DEEP, Block.SHALLOW),
                     (Block.DEEP, Block.FALLOW, Block.SHALLOW)]
        self.random = Random()
        self.fallow_area = 0.0
        self.shallow_area = 0.0
        self.deep_area = 0.0
        self.runtime = time.time()

    def search(self, colored_block_number) -> Optional[List[Block]]:
        if colored_block_number >= len(self.blocks):
            # All blocks are colored
            if self._check_total_area_():
                # Found a solution
                return self.blocks
            else:
                return None

        if time.time() - self.runtime > Parameter.MAX_SEARCH_TIME:
            return None

        # For Debug Only
        # if time.time() - self.runtime > 30:
        #     return self.blocks

        if colored_block_number == 0:
            block = self.random.choice(self.blocks)
        else:
            block = self._get_prior_block_()
        if block is None:
            return None

        neighbor_type = 0
        for neighbor in block.neighbors:
            neighbor_type = neighbor_type | neighbor.land_type

        if Parameter.CHOOSE_DEEP_FIRST:
            search_sequence = self.dice[1]
        else:
            search_sequence = self.random.choice(self.dice)
        for land_type in search_sequence:
            if land_type == Block.FALLOW:
                if (neighbor_type & Block.DEEP) > 0:
                    continue
                if block.area + self.fallow_area > self.total_area * Parameter.MAX_FALLOW_PORTION:
                    continue
                block.land_type = Block.FALLOW
                self.fallow_area += block.area
                if Operation.check_regions(self.blocks, self.total_area):
                    result = self.search(colored_block_number + 1)
                    if result is not None:
                        return result
                self.fallow_area -= block.area
                block.land_type = Block.BLANK
            elif land_type == Block.SHALLOW:
                if block.area + self.shallow_area > self.total_area * Parameter.MAX_SHALLOW_PORTION:
                    continue
                block.land_type = Block.SHALLOW
                self.shallow_area += block.area
                if Operation.check_regions(self.blocks, self.total_area):
                    result = self.search(colored_block_number + 1)
                    if result is not None:
                        return result
                self.shallow_area -= block.area
                block.land_type = Block.BLANK
            elif land_type == Block.DEEP:
                if block.at_edge:
                    continue
                if (neighbor_type & Block.FALLOW) > 0:
                    continue
                if block.area + self.deep_area > self.total_area * Parameter.MAX_DEEP_PORTION:
                    continue
                block.land_type = Block.DEEP
                self.deep_area += block.area
                if Operation.check_regions(self.blocks, self.total_area):
                    result = self.search(colored_block_number + 1)
                    if result is not None:
                        return result
                self.deep_area -= block.area
                block.land_type = Block.BLANK
        return None

    def _check_total_area_(self) -> bool:
        return (self.fallow_area >= self.total_area * Parameter.MIN_FALLOW_PORTION) and (
                self.shallow_area >= self.total_area * Parameter.MIN_SHALLOW_PORTION) and (
                       self.deep_area >= self.total_area * Parameter.MIN_DEEP_PORTION)

    def _get_prior_block_(self) -> Optional[Block]:
        prior_block = None  # type: Optional[Block]
        colored_neighbor_number = -1
        uncolored_neighbor_number = sys.maxsize
        adjacent_to_special_block = False
        for block in self.blocks:
            if block.land_type == Block.BLANK:
                colored_adj = 0
                uncolored_adj = 0
                special_adj = False
                for neighbor in block.neighbors:
                    if neighbor.land_type == Block.BLANK:
                        uncolored_adj += 1
                    else:
                        colored_adj += 1
                        if neighbor.land_type == Block.FALLOW or neighbor.land_type == Block.DEEP:
                            special_adj = True
                if prior_block is None or (
                        special_adj > adjacent_to_special_block or (
                        (special_adj == adjacent_to_special_block) and (
                        colored_adj > colored_neighbor_number or (
                        (colored_adj == colored_neighbor_number) and (
                        uncolored_adj < uncolored_neighbor_number))))):
                    prior_block = block
                    colored_neighbor_number = colored_adj
                    uncolored_neighbor_number = uncolored_adj
                    adjacent_to_special_block = special_adj
        return prior_block


class FileStream:

    @staticmethod
    def read_blocks_from_text(file_name: str) -> List[Block]:
        file = open(file_name, "r")
        lines = file.readlines()
        file.close()
        points = list()  # type: List[Point]
        blocks = list()  # type: List[Block]
        for line in lines:
            sub_str = line[line.find("{") + 1: line.find("}")]
            numbers = sub_str.split(",")
            if len(numbers) > 1:
                x = float(numbers[0].strip())
                y = float(numbers[1].strip())
                points.append(Point(x, y))
            elif len(points) > 0:
                blocks.append(Block(points))
                points = list()
        if len(points) > 0:
            blocks.append(Block(points))
            points = list()
        return blocks

    @staticmethod
    def save_blocks_to_image(blocks: List[Block], file_name: str) -> None:
        min_x, min_y, max_x, max_y = float("INF"), float("INF"), float("-INF"), float("-INF")
        for block in blocks:
            for point in block.points:
                max_x = max(max_x, point.x)
                min_x = min(min_x, point.x)
                max_y = max(max_y, point.y)
                min_y = min(min_y, point.y)
        cord_x = (max_x - min_x) * Parameter.IMAGE_RESOLUTION_SCALE
        cord_y = (max_y - min_y) * Parameter.IMAGE_RESOLUTION_SCALE
        image = Image.new("RGB", (int(cord_x), int(cord_y)), "white")
        drawer = ImageDraw.Draw(image)
        for block in blocks:
            FileStream._draw_block_(block, drawer, min_x, max_y)
        image.save(file_name)

    @staticmethod
    def _draw_block_(block: Block, drawer: ImageDraw, min_x: float, max_y: float) -> None:
        points = list()  # type: List[Tuple[float, float]]
        for point in block.points:
            x = (point.x - min_x) * Parameter.IMAGE_RESOLUTION_SCALE
            y = (max_y - point.y) * Parameter.IMAGE_RESOLUTION_SCALE
            points.append((x, y))
        if block.land_type == Block.FALLOW:
            color = Parameter.FALLOW_COLOR
        elif block.land_type == Block.SHALLOW:
            color = Parameter.SHALLOW_COLOR
        elif block.land_type == Block.DEEP:
            color = Parameter.DEEP_COLOR
        else:
            color = Parameter.BLANK_COLOR
        drawer.polygon(points, color, Parameter.OUTLINE_COLOR)


def run(all_blocks_file: str, edge_blocks_file: str, output_image_folder: str) -> None:
    all_blocks = FileStream.read_blocks_from_text(all_blocks_file)
    edge_blocks = FileStream.read_blocks_from_text(edge_blocks_file)
    Operation.mark_edge_blocks(all_blocks, edge_blocks)
    for i in range(Parameter.MAX_SOLUTION_NUM):
        search = Search(all_blocks)
        solution = search.search(0)
        if solution is not None:
            print("Found Solution in {0} Search".format(i + 1))
            file_name = os.path.join(output_image_folder, "{0}.png".format(time.time()))
            FileStream.save_blocks_to_image(solution, file_name)
        else:
            print("Search {0} Timeout".format(i + 1))
    print("All Search Completed")


if __name__ == "__main__":
    if len(sys.argv) == 4:
        run(sys.argv[1], sys.argv[2], sys.argv[3])
    else:
        print("""
        Usage: python ScriptFile AllBlocksFile EdgeBlocksFile ImageFolder
            -- ScriptFile:
                File name of this script, including path (relative or absolute)
            -- AllBlocksFile:
                File name of all blocks input, including path (relative or absolute)
            -- EdgeBlocksFile:
                File name of edge blocks input, including path (relative or absolute)
            -- ImageFolder:
                Path of the result image folder, must exists
        Example:
            python LandScape.py D:\\LandScape\\all_blocks.txt D:\\LandScape\\edge_blocks.txt D:\\LandScape\\Results\\
        """)
