package algorithm;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Operation {
	
	public static void generateAdjacents(List<Block> blocks) {
		int num = blocks.size();
		for(int i = 0; i < num - 1; ++i) {
			Block block1 = blocks.get(i);
			for (int j = i + 1; j < num; ++j) {
				Block block2 = blocks.get(j);
				if (Geometry.isAdjacent(block1, block2)) {
					block1.adjacents.add(block2);
					block2.adjacents.add(block1);
				}
			}
		}
	}
	
	public static boolean checkRegions(Collection<Block> usedBlocks, double totalArea) {
		List<Region> allRegions = getAllRegions(usedBlocks);
		for (Region region: allRegions) {
			if (!region.checkAreaLimit(totalArea)) {
				return false;
			}
		}
		return true;
	}
	
	public static List<Block> getBlocksFromFile(String fileName) {
		List<Block> blocks = new ArrayList<>();
		try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
			String line;
			while ((line = reader.readLine()) != null) {
				List<Point> points = new ArrayList<>();
				String[] numbers = line.split(",");
				for(int i = 0; i < numbers.length; i += 2) {
					double x = Double.parseDouble(numbers[i]);
					double y = Double.parseDouble(numbers[i + 1]);
					points.add(new Point(x, y));
				}
				Point[] pointsArray = points.toArray(new Point[points.size()]);
				blocks.add(new Block(pointsArray));
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return blocks;
	}
	
	public static void markEdgeBlocks(Collection<Block> allBlocks, Collection<Block> edgeBlocks) {
		for (Block block: allBlocks) {
			for (Block edgeBlock: edgeBlocks) {
				if (block.equals(edgeBlock)) {
					block.isEdge = true;
				}
			}
		}
	}
	
	private static List<Region> getAllRegions(Collection<Block> blocks) {
		Map<Block, Union> allUnions = new HashMap<>();
		for (Block block: blocks) {
			allUnions.put(block, new Union(block.type));
		}
		for (Entry<Block, Union> entry: allUnions.entrySet()) {
			Block block = entry.getKey();
			Union union = entry.getValue();
			for (Block neighbor: block.adjacents) {
				if (block.type == neighbor.type ||
						(block.type | neighbor.type) == Block.TYPE.SHALLOW) {
					union.merge(allUnions.get(neighbor));
				}
			}
		}
		Map<Union, List<Block>> unionBlocksMap = new HashMap<>();
		for (Entry<Block, Union> entry: allUnions.entrySet()) {
			Block block = entry.getKey();
			Union union = entry.getValue();
			Union root = union.root();
			if (!unionBlocksMap.containsKey(root)) {
				unionBlocksMap.put(root, new ArrayList<>());
			}
			unionBlocksMap.get(root).add(block);
		}
		List<Region> allRegions = new ArrayList<>();
		for (Entry<Union, List<Block>> entry: unionBlocksMap.entrySet()) {
			allRegions.add(new Region(entry.getKey().type, entry.getValue()));
		}
		return allRegions;
	}

}
