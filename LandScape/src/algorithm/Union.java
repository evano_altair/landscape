package algorithm;

public class Union {
	
	public Union parent;
	
	public int type;
	
	public Union(int type) {
		this.type = type;
		this.parent = null;
	}
	
	public Union root() {
		if (parent == null) {
			return this;
		}
		return parent = parent.root();
	}
	
	public void merge(Union region) {
//		if (this.type != region.type) {
//			return;
//		}
		Union root1 = this.root();
		Union root2 = region.root();
		if (!root1.equals(root2)) {
			root2.parent = root1;
		}
	}

}
