package algorithm;

import java.util.ArrayList;
import java.util.List;

public class Polygon {
	
	public final List<Line> lines;
	
	public final List<Point> points;
	
	public final double width;
	
	public final double area;
	
	public Polygon(Point... points) {
		this.lines = new ArrayList<>();
		this.points = new ArrayList<>();
		for (int i = 0; i < points.length; ++i) {
			this.points.add(points[i]);
			this.lines.add(new Line(points[i], points[(i + 1) % points.length]));
		}
		this.width = Double.MAX_VALUE;
//		this.width = getWidth();
		this.area = getArea();
	}
	
	private double getWidth() {
		double minimumWidth = Double.MAX_VALUE;
		Line[] ls = this.lines.toArray(new Line[this.lines.size()]);
		for (int i = 0; i < ls.length - 1; ++i) {
			Line l1 = ls[i];
			for (int j = i + 1; j < ls.length; ++j) {
				Line l2 = ls[j];
				if (Geometry.isConnected(l1, l2)) {
					continue;
				}
				double distance = Geometry.distance(l1, l2);
				if (minimumWidth > distance) {
					minimumWidth = distance;
				}
			}
		}
		return minimumWidth;
	}
	
	private double getArea() {
		Point[] data = this.points.toArray(new Point[this.points.size()]);
		double sum = data[0].y * (data[data.length - 1].x - data[1].x);
		for (int i = 1; i < data.length; ++i) {
			sum += data[i].y * (data[i - 1].x - data[(i + 1) % data.length].x);
		}
		return Math.abs(sum / 2);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Polygon)) {
			return false;
		}
		Polygon other = (Polygon) obj;
		if (points == null) {
			if (other.points != null) {
				return false;
			}
		} else {
			if (other.points == null) {
				return false;
			}
		}
		if (points.size() != other.points.size()) {
			return false;
		}
		int foundPoints = 0;
		for (Point point: points) {
			for (Point otherPoint: other.points) {
				if (point.equals(otherPoint)) {
					++foundPoints;
				}
			}
		}
		return foundPoints == points.size();
	}
	
}
