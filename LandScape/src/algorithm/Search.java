package algorithm;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class Search {

	public List<Solution> solutions;

	private final List<Block> blocks;

//	private final Set<Block> usedBlocks;

	private final Random random;

	private final double totalArea;

	private int validSolutionNum = 0;

	private double fallowArea = 0;
	private double shallowArea = 0;
	private double deepArea = 0;
	
	private long runtime;
	
	@SuppressWarnings("boxing")
	private final List<Integer []> dice = Arrays.asList(
			new Integer[] {Block.TYPE.FALLOW, Block.TYPE.SHALLOW, Block.TYPE.DEEP},
			new Integer[] {Block.TYPE.FALLOW, Block.TYPE.DEEP, Block.TYPE.SHALLOW},
			new Integer[] {Block.TYPE.SHALLOW, Block.TYPE.FALLOW, Block.TYPE.DEEP},
			new Integer[] {Block.TYPE.SHALLOW, Block.TYPE.DEEP, Block.TYPE.FALLOW},
			new Integer[] {Block.TYPE.DEEP, Block.TYPE.FALLOW, Block.TYPE.SHALLOW},
			new Integer[] {Block.TYPE.DEEP, Block.TYPE.SHALLOW, Block.TYPE.FALLOW});
	
	private final int id;

	public Search(List<Block> blocks, int id) {
		this.id = id;
		this.blocks = blocks;
//		this.usedBlocks = new HashSet<>();
		this.totalArea = getTotalArea();
		this.random = new Random();
		this.runtime = System.currentTimeMillis();
	}

	@SuppressWarnings("boxing")
	public void search(int index) {
		if (index >= blocks.size()) {
			if (checkArea()) {
				System.out.println("Thread " + id + " Got Solution " + ++validSolutionNum);
				if (validSolutionNum % 100000 == 1) {
					Solution.saveSolution(blocks);
				} else if (System.currentTimeMillis() - runtime > 60000) {
					Solution.saveSolution(blocks);
				}
				runtime = System.currentTimeMillis();
			}
			return;
		}
		
		if (validSolutionNum == 1) {
			return;
		}
		
//		if (System.currentTimeMillis() - runtime > 30000) {
//			Solution.saveSolution(blocks);
//			runtime = System.currentTimeMillis();
//		}
		
		Block block = null;
		if (index == 0) {
			block = blocks.get(0);
		} else {
			block = getPriorBlock();
		}
		if (block == null) {
			return;
		}
		int adjacentType = 0;
		for (Block neighbor : block.adjacents) {
			adjacentType = adjacentType | neighbor.type;
		}
		
//		Integer[] sequence = dice.get(random.nextInt(6));
//		Integer[] sequence = dice.get(1);
		Integer[] sequence = dice.get(1 + 3 * random.nextInt(2));
//		Integer[] sequence = null;
//		if (block.isEdge) {
//			sequence = dice.get(2 * random.nextInt(2));
//		} else {
//			sequence = dice.get(1 + 3 * random.nextInt(2));
//		}
		
		for (int type : sequence) {
			if (type == Block.TYPE.FALLOW) {
				if (block.width < Parameters.MIN_FALLOW_WIDTH) {
					continue;
				}
				if ((adjacentType & Block.TYPE.DEEP) > 0) {
					continue;
				}
				if (block.area + fallowArea > totalArea * Parameters.MAX_FALLOW_PORTION) {
					continue;
				}
				block.type = Block.TYPE.FALLOW;
				fallowArea += block.area;
//				usedBlocks.add(block);
				if (Operation.checkRegions(blocks, totalArea)) {
					search(index + 1);
				}
//				usedBlocks.remove(block);
				fallowArea -= block.area;
				block.type = Block.TYPE.BLANK;
			} else if (type == Block.TYPE.SHALLOW) {
				if (block.width < Parameters.MIN_SHALLOW_WIDTH) {
					continue;
				}
				if (block.area + shallowArea > totalArea * Parameters.MAX_SHALLOW_PORTION) {
					continue;
				}
				block.type = Block.TYPE.SHALLOW;
				shallowArea += block.area;
//				usedBlocks.add(block);
				if (Operation.checkRegions(blocks, totalArea)) {
					search(index + 1);
				}
//				usedBlocks.remove(block);
				shallowArea -= block.area;
				block.type = Block.TYPE.BLANK;
			} else if (type == Block.TYPE.DEEP) {
				if (block.isEdge) {
					continue;
				}
				if (block.width < Parameters.MIN_DEEP_WIDTH) {
					continue;
				}
				if ((adjacentType & Block.TYPE.FALLOW) > 0) {
					continue;
				}
				if (block.area + deepArea > totalArea * Parameters.MAX_DEEP_PORTION) {
					continue;
				}
				block.type = Block.TYPE.DEEP;
				deepArea += block.area;
//				usedBlocks.add(block);
				if (Operation.checkRegions(blocks, totalArea)) {
					search(index + 1);
				}
//				usedBlocks.remove(block);
				deepArea -= block.area;
				block.type = Block.TYPE.BLANK;
			}
		}
	}

	private Block getPriorBlock() {
		Block priorBlock = null;
		int coloredAdjacentNum = -1;
		int uncoloredAdjacentNum = Integer.MAX_VALUE;
		boolean isSpecialAdjcent = false;
		for (Block block : blocks) {
			if (block.type == Block.TYPE.BLANK) {
				int coloredAdjNum = 0;
				int uncoloredAdjNum = 0;
				boolean isSpecialAdj = false;
				for (Block neighbor : block.adjacents) {
					if (neighbor.type == Block.TYPE.BLANK) {
						++uncoloredAdjNum;
					} else {
						++coloredAdjNum;
						if (neighbor.type == Block.TYPE.FALLOW || neighbor.type == Block.TYPE.DEEP) {
							isSpecialAdj = true;
						}
					}
				}
				if (priorBlock == null ||
//						((block.isEdge && !priorBlock.isEdge) ||
//						(block.isEdge == priorBlock.isEdge &&
						((isSpecialAdj && !isSpecialAdjcent) ||
						(isSpecialAdj == isSpecialAdjcent &&
						(coloredAdjNum > coloredAdjacentNum ||
						(coloredAdjNum == coloredAdjacentNum &&
						uncoloredAdjNum < uncoloredAdjacentNum))))) {
					priorBlock = block;
					coloredAdjacentNum = coloredAdjNum;
					uncoloredAdjacentNum = uncoloredAdjNum;
					isSpecialAdjcent = isSpecialAdj;
				}
			}
		}
		return priorBlock;
	}

	private double getTotalArea() {
		double area = 0;
		for (Block block : blocks) {
			area += block.area;
		}
		return area;
	}

	private boolean checkArea() {
		return fallowArea >= totalArea * Parameters.MIN_FALLOW_PORTION
				&& shallowArea >= totalArea * Parameters.MIN_SHALLOW_PORTION
				&& deepArea >= totalArea * Parameters.MIN_DEEP_PORTION;
	}



}
