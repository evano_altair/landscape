package algorithm;

import java.util.List;

public class Main {

	public static void main(String[] args) {
		String allBlocksFile = args[0];
		String edgeBlocksFile = args[1];
		List<Block> allBlocks = Operation.getBlocksFromFile(allBlocksFile);
		List<Block> edgeBlocks = Operation.getBlocksFromFile(edgeBlocksFile);
		Operation.markEdgeBlocks(allBlocks, edgeBlocks);
		for (int i = 0; i < Parameters.THREAD_NUM; ++i) {
			RunThread thread = new RunThread(allBlocks, i);
			thread.start();
		}
	}

}
