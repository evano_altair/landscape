package algorithm;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Region {
	
	private final Set<Block> blocks;
	
	public final int type;
	
	public Region(int type, List<Block> blocks) {
		this.type = type;
		this.blocks = new HashSet<>(blocks);
	}
	
	public boolean isClosed() {
		for (Block block: blocks) {
			for (Block neighbor: block.adjacents) {
				if (neighbor.type == Block.TYPE.BLANK && !blocks.contains(neighbor)) {
					return false;
				}
			}
		}
		return true;
	}
	
	public boolean checkAreaLimit(double totalArea) {
		double minArea = 0;
		double maxArea = Double.MAX_VALUE;
		double area = 0;
		if (type == Block.TYPE.FALLOW) {
			minArea = Parameters.MIN_FALLOW_AREA;
			maxArea = Parameters.MAX_FALLOW_AREA;
		} else if (type == Block.TYPE.DEEP) {
			minArea = Parameters.MIN_DEEP_AREA;
			maxArea = Parameters.MAX_DEEP_AREA;
		} else {
			minArea = Math.max(Parameters.MIN_SHALLOW_AREA, totalArea * Parameters.MIN_SHALLOW_PORTION);
			maxArea = Parameters.MAX_SHALLOW_AREA;
		}
		for (Block block: blocks) {
			area += block.area;
		}
		if (area > maxArea) {
			return false;
		} else if (isClosed() && area < minArea) {
			return false;
		}
		return true;
	}

}
