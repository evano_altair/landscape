package algorithm;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Solution {
	
	private final List<Block> blocks;
	
	public Solution(List<Block> blocks) {
		this.blocks = new ArrayList<>();
		for (Block block: blocks) {
			Point[] pointsArray = block.points.toArray(new Point[block.points.size()]);
			this.blocks.add(new Block(pointsArray));
		}
	}
	
	public static void saveSolution(List<Block> blocks) {
		String fileName;
		fileName = Parameters.SOLUTION_SAVE_PATH + "/" + System.nanoTime() + ".txt";
		int blockId = 0;
		String typeName;
		try (BufferedWriter file = new BufferedWriter(new FileWriter(fileName))) {
			for (Block block : blocks) {
				if (block.type == Block.TYPE.FALLOW) {
					typeName = "Fallow";
				} else if (block.type == Block.TYPE.SHALLOW) {
					typeName = "Shallow";
				} else if (block.type == Block.TYPE.DEEP){
					typeName = "Deep";
				} else {
					typeName = "Blank";
				}
				file.write("Block_" + (blockId++) + " ");
				file.write(typeName + " ");
				for (Point point : block.points) {
					file.write(point.x + " " + point.y + " ");
				}
				file.write("\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
