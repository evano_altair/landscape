package algorithm;

public class Parameters {
	
	public static final double MIN_FALLOW_WIDTH = 3;
	public static final double MIN_SHALLOW_WIDTH = 6;
	public static final double MIN_DEEP_WIDTH = 8;
	
	public static final double MIN_FALLOW_AREA = 2000;
	public static final double MIN_SHALLOW_AREA = 20000;
	public static final double MIN_DEEP_AREA = 6000;
	public static final double MAX_FALLOW_AREA = 12000;
	public static final double MAX_SHALLOW_AREA = Double.MAX_VALUE;
	public static final double MAX_DEEP_AREA = 24000;
	
	public static final double MIN_FALLOW_PORTION = 0.15;
	public static final double MIN_SHALLOW_PORTION = 0.45;
	public static final double MIN_DEEP_PORTION = 0.25;
	public static final double MAX_FALLOW_PORTION = 0.225;
	public static final double MAX_SHALLOW_PORTION = 0.525;
	public static final double MAX_DEEP_PORTION = 0.325;
	
	public static final double DOUBLE_PRECISION = 1E-9;
	public static final String SOLUTION_SAVE_PATH = "/localworkspaces/Solutions";
	public static final String SOLUTION_DEBUG_PATH = "/localworkspaces/Debug";
	public static final int THREAD_NUM = 100;

}
