package algorithm;

public class Geometry {

	public static double distance(Point a, Point b) {
		return Math.sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
	}

	public static double distance(Point p, Line l) {
		Point a = l.a;
		Point b = l.b;
		double r = ((p.x - a.x) * (b.x - a.x) + (p.y - a.y) * (b.y - a.y))
				/ ((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
		if (r <= 0) {
			return distance(a, p);
		} else if (r >= 1) {
			return distance(b, p);
		} else {
			double ab = distance(a, b);
			double ac = r * ab;
			double ap = distance(a, p);
			return Math.sqrt(ap * ap - ac * ac);
		}
	}

	public static double distance(Line l1, Line l2) {
		double min = Double.MAX_VALUE;
		double d1 = distance(l1.a, l2);
		if (min > d1) {
			min = d1;
		}
		double d2 = distance(l1.b, l2);
		if (min > d2) {
			min = d2;
		}
		double d3 = distance(l2.a, l1);
		if (min > d3) {
			min = d3;
		}
		double d4 = distance(l2.b, l1);
		if (min > d4) {
			min = d4;
		}
		return min;
	}
	
	public static boolean isConnected(Line l1, Line l2) {
		return l1.a.equals(l2.a) || l1.a.equals(l2.b) || l1.b.equals(l2.a) || l1.b.equals(l2.b);
	}
	
	public static boolean isAdjacent(Polygon p1, Polygon p2) {
		for (Line l1: p1.lines) {
			for (Line l2: p2.lines) {
				if (l1.equals(l2)) {
					return true;
				}
			}
		}
		return false;
	}

}
