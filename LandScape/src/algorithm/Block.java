package algorithm;

import java.util.ArrayList;
import java.util.List;

public class Block extends Polygon {
	
	public static final class TYPE {
		public static final int BLANK = 0b000;
		public static final int FALLOW = 0b001;
		public static final int SHALLOW = 0b010;
		public static final int DEEP = 0b100;
	}
	
	public int type;
	
	public boolean isEdge;
	
	public List<Block> adjacents;
	
	public Block(Point... points) {
		super(points);
		this.type = TYPE.BLANK;
		this.isEdge = false;
		this.adjacents = new ArrayList<>();
	}

}
