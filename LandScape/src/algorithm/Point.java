package algorithm;

public class Point{
	
	public final double x;
	
	public final double y;
	
	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Point)) {
			return false;
		}
		Point other = (Point) obj;
		if (Math.abs(x - other.x) > Parameters.DOUBLE_PRECISION) {
			return false;
		}
		if (Math.abs(y - other.y) > Parameters.DOUBLE_PRECISION) {
			return false;
		}
		return true;
	}


}
