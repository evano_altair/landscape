package algorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RunThread extends Thread {
	
	private final List<Block> blocks;
	
	private final int index;
	
	public RunThread(List<Block> blocks, int index) {
		this.index = index;
		this.blocks = new ArrayList<>();
		for (Block block: blocks) {
			Point[] points = block.points.toArray(new Point[block.points.size()]);
			Block newBlock = new Block(points);
			newBlock.isEdge = block.isEdge;
			this.blocks.add(newBlock);
		}
		Operation.generateAdjacents(this.blocks);
	}
	
	@Override
	public void run() {
		Collections.shuffle(blocks);
		Search search = new Search(blocks, index);
		search.search(0);
	}

}
