package algorithm;

public class Line {
	
	public final Point a;
	
	public final Point b;
	
	public Line(Point a, Point b) {
		this.a = a;
		this.b = b;
	}
	
	public Line(double x1, double y1, double x2, double y2) {
		this.a = new Point(x1, y1);
		this.b = new Point(x2, y2);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Line)) {
			return false;
		}
		Line other = (Line) obj;
		if (a == null) {
			if (other.a != null) {
				return false;
			}
		}
		if (b == null) {
			if (other.b != null) {
				return false;
			}
		}
		return (a.equals(other.a) && b.equals(other.b)) || (a.equals(other.b) && b.equals(other.a));
	}

}
